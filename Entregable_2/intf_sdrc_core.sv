interface intf_sdrc_core(input clk);
  
  parameter      dw              = 16;  // data width
  
/***********************************************************
                  Create Conexions for Core
************************************************************/
/*
  -app_req         --   Application Request, 1 when is a write or read
  -app_req_len     --   Burst Request length
  -app_req_ack     --   Application Request Ack
  -app_req_addr    --   Application Address
  -app_req_wr_n    --   Set a 1 to Read or 0 to write
  -app_wr_data     --   Data to write
  -app_wr_en_n     --   Enable Write
  -app_rd_valid    --   The read is valid or not
  -app_last_rd     --   Lat valid read
  -app_last_wr     --   Last valid write
  -app_rd_data     --   SDRAM Init Done
  -app_wr_next_req --
  -sdram_clk = clk
  -RESETN
*/  
  
  logic                   app_req            ; 
  logic [8:0]             app_req_len        ; 
  logic                   app_req_ack        ; 
  logic [25:0]            app_req_addr       ; 
  logic                   app_req_wr_n       ; 
  logic [31:0]            app_wr_data        ; 
  logic [3:0]             app_wr_en_n        ; 
  logic                   app_rd_valid       ; 
  logic                   app_last_rd        ; 
  logic                   app_last_wr        ; 
  logic [31:0]            app_rd_data        ; 
  logic                   app_wr_next_req    ;
  logic                   sdram_clk = clk    ;
  logic                   RESETN             ;
  logic test;
  
  
/***********************************************************
                  Create Conexions for SDRAM
************************************************************/
/*
  -DQ            --   SDRAM Read/Write Data Bus
  -sdr_dout      --   SDRAM Data Out
  -pad_sdr_din   --   SDRAM Data Input
  -sdr_den_n     --   SDRAM Data Enable
  -sdr_dqm       --   SDRAM DATA Mask
  -sdr_ba        --   SDRAM Bank Select
  -sdr_addr      --   SDRAM ADRESS
  -sdr_init_done --   SDRAM Init Done
  -app_busy_n    --   0 -> sdr busy
*/
  wire [dw-1:0]            Dq                 ; 
  wire [dw-1:0]            sdr_dout           ;  
  wire [dw/8-1:0]          sdr_den_n          ; 
  wire [dw/8-1:0]          sdr_dqm            ;
  wire [1:0]               sdr_ba             ; 
  wire [12:0]              sdr_addr           ; 
  wire                     sdr_init_done      ;
  wire                     app_busy_n         ; 
  
  
/***********************************************************
                  Create Conexions for WISH BONE Interface
************************************************************/


  logic                    wb_stb_i           ;
  wire                     wb_ack_o           ;
  logic [25:0]             wb_addr_i          ;
  logic                    wb_we_i            ; 
  logic [dw-1:0]           wb_dat_i           ;
  logic [dw/8-1:0]         wb_sel_i           ; 
  wire  [dw-1:0]           wb_dat_o           ;
  logic                    wb_cyc_i           ;
  logic [2:0]              wb_cti_i           ;
  
endinterface