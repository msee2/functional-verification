class coverage;
  virtual intf_sdrc_core intfc;

  /* Read group */
  covergroup cov_read @(posedge intfc.clk);
    Feature_req_ack: coverpoint intfc.app_req_ack {
      bins t1_req_ack = {1};
    }
    Feature_app_rd_valid : coverpoint intfc.app_rd_valid {
      bins t1_read = (0 => 1), (1 => 0);
    } 
    Feature_add_wr_n: coverpoint intfc.app_req_wr_n {
      bins t1_write_pin = {1};
    }
    Feature_effective_req_read: cross Feature_add_wr_n, Feature_req_ack;
  endgroup

  /* Write group */
  covergroup cov_write @(posedge intfc.clk);
    Feature_req_ack: coverpoint intfc.app_req_ack {
      bins t2_ack = {1};
    }
    Feature_req: coverpoint intfc.app_req {
      bins t2_req = (0 => 1), (1 => 0);
    }
    Feature_add_wr_n: coverpoint intfc.app_req_wr_n {
      bins t2_write_pin = {0};
    }
    Feature_effective_req_write: cross Feature_add_wr_n, Feature_req_ack, Feature_req;
  endgroup

  /* Bulk writes */
  covergroup cov_bulk_write @(posedge intfc.clk);
    Feature_req_len: coverpoint intfc.app_req_len {
      bins t3_len[] = {[1:$]};
    }
  endgroup

  /* Constructor */
  function new (virtual intf_sdrc_core intfc_);
    this.intfc = intfc_;
    cov_read = new();
    cov_write = new();
    cov_bulk_write = new();
  endfunction

endclass
