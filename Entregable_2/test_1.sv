program testcase(intf_sdrc_core intfc);
  environment env = new(intfc);
  logic [31:0] data;
  logic delay_1;
  logic delay_2;
  logic delay_3;
  
  initial
    begin
      env.drvr.sti=new();
      if(env.drvr.sti.randomize())
        begin
          delay_1 =   env.drvr.sti.delay_5;
          delay_2 =   env.drvr.sti.delay_6;
          delay_3 =   env.drvr.sti.delay_7;
        end
      env.drvr.start();
      fork
        begin
          env.drvr.write(5,1);
          @(negedge intfc.clk);
          env.drvr.read(8);
          #(delay_1);
        end
      join
      #(delay_2);
      $display("Errors: %d, Checks: %d", env.mntr.err_count, env.mntr.check_count);
        if (env.mntr.err_count == 0 && env.mntr.check_count > 0)
          $display("\n***** Test Passed *****\n");
        else
          $display("\n***** Test Failed *****\n");
      #(delay_3);
    end
endprogram