#!/bin/bash
PATH=/mnt/vol_NFS_Zener/tools/synopsys/apps/vcs-mx2/M-2017.03-SP2-5/bin:${PATH}

source s1.bash
vcs '-timescale=1ns/1ns' design.sv testbench.sv -sverilog -full64 -debug_access+all -cm line+tgl+assert
./simv -cm line+tgl+assert
urg -full64 -dir simv.vdb
firefox urgReport/dashboard.html
