	class monitor;
  
  logic[31:0]  dutdata;
  logic[31:0]  sbdata;
  logic[31:0]  address;
  logic[31:0]  addressw;
  
  scoreboard sb;
  virtual intf_sdrc_core intfc;

  int err_count ;
  int check_count;
          
  function new(virtual intf_sdrc_core intfc,scoreboard sb);
    this.intfc = intfc;
    this.sb = sb;
  endfunction
  
  /* Checker is integrated within the monitor! */
  task check();
    err_count = 0;
    check_count = 0;
    $display("Executing checks...:");
    forever
      begin
        @ (posedge intfc.clk)
          if (intfc.app_req == 1)
            begin
              check_count++;
              if (intfc.app_req_wr_n== 0)
                begin
                  @ (posedge intfc.app_req)
              	  $display("Escritura");
                  sb.dram_w[(intfc.app_req_addr) & 32'hFFFFFFFF] = intfc.app_wr_data; // Write data into the scoreboard
                  sb.afifo.push_back(intfc.app_req_addr);
                end
              else
                begin
                  @ (posedge intfc.app_rd_valid)
                  $display("Lectura");
                  address = intfc.app_req_addr;
                  sbdata = sb.dram_w[address];
                  dutdata = intfc.app_rd_data;
                  addressw = sb.afifo.pop_front();
                  if (addressw == address)
                    begin
                      $display(" * Test %x", addressw);
                      if (sbdata !== dutdata)
                        begin
                          $display(" * ERROR * DUT data is %x :: SB data is %x [Address: %x]", dutdata, sbdata, address);
                          err_count++;
                        end
                      else
                        begin
                          $display(" * PASS * DUT data is %x :: SB data is %x [Address: %x]", dutdata, sbdata, address);
                        end
                    end
              	end
            end
      end
  endtask
endclass
