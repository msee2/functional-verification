#!/bin/bash
source s1.bash
vcs '-timescale=1ns/1ns' design.sv testbench.sv -sverilog -full64 -debug_access+all
./simv -cm line+tgl+assert
