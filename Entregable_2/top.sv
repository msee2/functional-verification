`timescale 1ns/1ps

module top();
  
  //Define Byte Lenght
  `define S16;
  
  parameter      P_SYS  = 10;     //    100MHz
  parameter      Byte_Lenght = 2; // 1. 32 Bits - 2. 16 Bits - 3. 8 Bits
  parameter      dw              = 16;  // data width
  parameter      tw              = 8;   // tag id width
  parameter      bl       = 9;   // burst_lenght_width
  reg            sdram_clk;       // Global CLK
  
  //Set Clk for SDRAm to 0 (mt)
  initial sdram_clk = 0;
  
  //Generate CLK for SDRAM
  always #(P_SYS/2) sdram_clk = !sdram_clk;
  
  //Generate the interface to test
  `ifdef S32
    intf_sdrc_core #(.dw(32)) intfc (.clk (sdram_clk));
  `elsif S16
    intf_sdrc_core #(.dw(16)) intfc (.clk (sdram_clk));
  `else
    intf_sdrc_core #(.dw(8)) intfc (.clk (sdram_clk));
  `endif
  
  // to fix the sdram interface timing issue
  wire #(2.0) sdram_clk_d = sdram_clk;
  wire #(1.0) pad_clk     = sdram_clk_d; 
  
  /***********************************************************
                  Set Conexions for Controller
  ************************************************************/
  /*
  -General connections -- Define the inputs for the controller to work, clk, reset or clk for SDRAM
  -Request signals for controllers -- Determine the signals need to create a write or read request
  -Interface signals for SDRAM -- Set the signals to send to the SDRAM
  -Parameters for SDRAM
  */
  
  `ifdef S32
     sdrc_core #(.SDR_DW(32),.SDR_BW(4)) 
     u_dut(
  `elsif S16
     sdrc_core #(.SDR_DW(16),.SDR_BW(2)) 
     u_dut(
  `else 
     sdrc_core #(.SDR_DW(8),.SDR_BW(1)) 
     u_dut(
  `endif
       
     /*General connections*/
       
           .clk                (sdram_clk               ),
           .reset_n            (intfc.RESETN            ),
           .pad_clk            (pad_clk                 ), 
          `ifdef S32
             .sdr_width        (2'b00                   ),
          `elsif S16
             .sdr_width        (2'b01                   ),
          `else 
             .sdr_width        (2'b10                   ),
          `endif
          .cfg_colbits         (2'b00                   ), 
    /* Request signals for controllers */
          .app_req            (intfc.app_req            ),	
          .app_req_addr       (intfc.app_req_addr       ),	
          .app_req_len        (intfc.app_req_len        ),	
          .app_req_wrap       (1'b0                     ),	
          .app_req_wr_n       (intfc.app_req_wr_n       ),	
          .app_req_ack        (intfc.app_req_ack        ),	
          .app_wr_data        (intfc.app_wr_data        ),
          .app_wr_en_n        (intfc.app_wr_en_n        ),
          .app_rd_data        (intfc.app_rd_data        ),
          .app_last_rd        (intfc.app_last_rd        ),
          .app_last_wr        (intfc.app_last_wr        ),
          .app_rd_valid       (intfc.app_rd_valid       ),
          .app_wr_next_req    (intfc.app_wr_next_req    ),
          .app_req_dma_last   (intfc.app_req            ),
   /* Interface signals for SDRAM */
          .sdr_cs_n           (sdr_cs_n                 ),
          .sdr_cke            (sdr_cke                  ),
          .sdr_ras_n          (sdr_ras_n                ),
          .sdr_cas_n          (sdr_cas_n                ),
          .sdr_we_n           (sdr_we_n                 ),
          .sdr_ba             (intfc.sdr_ba             ),
          .sdr_addr           (intfc.sdr_addr           ),
          .sdr_init_done      (intfc.sdr_init_done      ),
          .sdr_dqm            (intfc.sdr_dqm            ),
          .pad_sdr_din        (intfc.Dq                 ),
          .sdr_dout           (intfc.sdr_dout           ),
          .sdr_den_n          (intfc.sdr_den_n          ),
   /* Parameters for SDRAM */
          .cfg_req_depth      (2'h3                     ),
          .cfg_sdr_en         (1'b1                     ),
          .cfg_sdr_mode_reg   (13'h033                  ),
          .cfg_sdr_tras_d     (4'h4                     ),
          .cfg_sdr_trp_d      (4'h2                     ),
          .cfg_sdr_trcd_d     (4'h2                     ),
          .cfg_sdr_cas        (3'h3                     ),
          .cfg_sdr_trcar_d    (4'h7                     ),
          .cfg_sdr_twr_d      (4'h1                     ),
          .cfg_sdr_rfsh       (12'h100                  ), 
          .cfg_sdr_rfmax      (3'h6                     )
  );
       
  /*wb2sdrc #(.dw(dw),.tw(tw),.bl(bl)) u_wb2sdrc (
          .wb_rst_i           (!intfc.RESETN           ) ,
          .wb_clk_i           (sdram_clk            ) ,
          .wb_stb_i           (intfc.wb_stb_i           ) ,
          .wb_ack_o           (intfc.wb_ack_o           ) ,
          .wb_addr_i          (intfc.wb_addr_i          ) ,
          .wb_we_i            (intfc.wb_we_i            ) ,
          .wb_dat_i           (intfc.wb_dat_i           ) ,
          .wb_sel_i           (intfc.wb_sel_i           ) ,
          .wb_dat_o           (intfc.wb_dat_o           ) ,
          .wb_cyc_i           (intfc.wb_cyc_i           ) ,
          .wb_cti_i           (intfc.wb_cti_i           ) , 


      //SDRAM Controller Hand-Shake Signal 
          .sdram_clk          (sdram_clk          ) ,
          .sdram_resetn       (RESETN      ) ,
          .sdr_req            (intfc.app_req            ) ,
          .sdr_req_addr       (intfc.app_req_addr       ) ,
          .sdr_req_len        (intfc.app_req_len        ) ,
          .sdr_req_wr_n       (intfc.app_req_wr_n       ) ,
          .sdr_req_ack        (intfc.app_req_ack        ) ,
          .sdr_busy_n         (intfc.app_busy_n         ) ,
          .sdr_wr_en_n        (intfc.app_wr_en_n        ) ,
          .sdr_wr_next        (intfc.app_wr_next_req    ) ,
          .sdr_rd_valid       (intfc.app_rd_valid       ) ,
          .sdr_last_rd        (intfc.app_last_rd        ) ,
          .sdr_wr_data        (intfc.app_wr_data        ) ,
          .sdr_rd_data        (intfc.app_rd_data        ) 

      );*/
  
     
  `ifdef S32
    assign intfc.Dq[7:0]    = (intfc.sdr_den_n[0] == 1'b0) ? intfc.sdr_dout[7:0]   : 8'hZZ;
    assign intfc.Dq[15:8]   = (intfc.sdr_den_n[1] == 1'b0) ? intfc.sdr_dout[15:8]  : 8'hZZ;
    assign intfc.Dq[23:16]  = (intfc.sdr_den_n[2] == 1'b0) ? intfc.sdr_dout[23:16] : 8'hZZ;
    assign intfc.Dq[31:24]  = (intfc.sdr_den_n[3] == 1'b0) ? intfc.sdr_dout[31:24] : 8'hZZ;
       
    mt48lc2m32b2 #(.data_bits(32)) u_sdram32 (
          .Dq                 (intfc.Dq                 ), 
          .Addr               (intfc.sdr_addr[10:0]     ), 
          .Ba                 (intfc.sdr_ba             ), 
          .Clk                (sdram_clk_d              ), 
          .Cke                (sdr_cke                  ), 
          .Cs_n               (sdr_cs_n                 ), 
          .Ras_n              (sdr_ras_n                ), 
          .Cas_n              (sdr_cas_n                ), 
          .We_n               (sdr_we_n                 ), 
          .Dqm                (intfc.sdr_dqm            )
     );

  `elsif S16
    assign intfc.Dq[7:0]  = (intfc.sdr_den_n[0] == 1'b0) ? intfc.sdr_dout[7:0]  : 8'hZZ;
    assign intfc.Dq[15:8] = (intfc.sdr_den_n[1] == 1'b0) ? intfc.sdr_dout[15:8] : 8'hZZ;

    IS42VM16400K u_sdram16 (
         .dq                 (intfc.Dq                  ), 
         .addr               (intfc.sdr_addr[11:0]      ), 
         .ba                 (intfc.sdr_ba              ), 
         .clk                (sdram_clk_d               ), 
         .cke                (sdr_cke                   ), 
         .csb                (sdr_cs_n                  ), 
         .rasb               (sdr_ras_n                 ), 
         .casb               (sdr_cas_n                 ), 
         .web                (sdr_we_n                  ), 
         .dqm                (intfc.sdr_dqm             )
    );
       
  `else 
    assign intfc.Dq[7:0]  = (intfc.sdr_den_n[0] == 1'b0) ? intfc.sdr_dout[7:0]  : 8'hZZ;

    mt48lc8m8a2 #(.data_bits(8)) u_sdram8 (
          .Dq                 (intfc.Dq                 ), 
          .Addr               (intfc.sdr_addr[11:0]     ), 
          .Ba                 (intfc.sdr_ba             ), 
          .Clk                (sdram_clk_d              ), 
          .Cke                (sdr_cke                  ), 
          .Cs_n               (sdr_cs_n                 ), 
          .Ras_n              (sdr_ras_n                ), 
          .Cas_n              (sdr_cas_n                ), 
          .We_n               (sdr_we_n                 ), 
          .Dqm                (intfc.sdr_dqm            )
     );
`endif
       
       
  initial begin
    $dumpfile("verilog.vcd");
    $dumpvars(0);
  end
     
       testcase test(intfc);

endmodule