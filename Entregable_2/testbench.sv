/***********************************************************
                  Include the files 
************************************************************/
  
`include "intf_sdrc_core.sv"
`include "coverage.sv"
`include "stimulus.sv"
`include "scoreboard.sv"
`include "driver.sv"
`include "monitor.sv"
`include "env.sv"
`include "test_1.sv"
`include "top.sv"
