class stimulus;
  
  /*Random variables to test DUT*/
  rand logic [31:0] startAddr = {row,bank,column,2'b00} ;
  rand logic [31:0] data      = data & 32'hFFFFFFFF     ;
  rand logic [7:0]  bl        = bl & 8'hFf              ;
  rand logic [11:0] row       = row & 12'hFFF           ;
  rand logic [2:0]  bank      = bank & 2'b11            ;
  rand logic [11:0] column    = column & 8'hFF          ;
  
  /*Random delays for driver*/
  rand int          delay_1     = delay_1;
  rand int          delay_2     = delay_2;
  rand int          delay_3     = delay_3;
  rand int          delay_4     = delay_4;
  
  /*Random delay for test*/
  rand int          delay_5     = delay_5;
  rand int          delay_6     = delay_6;
  rand int          delay_7     = delay_7;
  
  constraint const_startAddr   {startAddr inside {[0:10000]}; }
  constraint const_data        {data inside {[0:10000]}; }
  constraint const_bl          {bl inside {[2:9]}; } 
  constraint const_row         {row inside {[0:4095]}; }
  constraint const_bank        {bank inside {[0:3]}; }
  constraint const_column      {column inside {[0:255]}; }
  constraint const_delay_1     {delay_1 inside {[0:100]}; }
  constraint const_delay_2     {delay_2 inside {[0:100]}; }
  constraint const_delay_3     {delay_3 inside {[0:100]}; }
  constraint const_delay_4     {delay_4 inside {[0:100]}; }
  constraint const_delay_5     {delay_5 inside {[50:300]}; }
  constraint const_delay_6     {delay_6 inside {[50:300]}; }
  constraint const_delay_7     {delay_7 inside {[50:300]}; }
  
  

endclass
