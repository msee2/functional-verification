class driver;
  stimulus sti;
  scoreboard sb;
  
  logic[31:0] address;
  logic[31:0] data;
  logic[31:0] bl;
  
  int i,j,k,x;
  int delay_1;
  int delay_2;
  int delay_3;
  int delay_4;
 
  virtual intf_sdrc_core intfc;
        
  function new(virtual intf_sdrc_core intfc,scoreboard sb);
    this.intfc = intfc;
    this.sb = sb;
  endfunction
  
/***********************************************************
                  Task reset the RAM 
************************************************************/
  
  //Reset all interfaces
  task start();  // start method
    begin
      sti=new();
      if(sti.randomize())
        begin
          delay_1 = sti.delay_1;
          delay_2 = sti.delay_2;
          delay_3 = sti.delay_3;
          delay_4 = sti.delay_4;
        end
      $display("Executing start\n");
      intfc.app_req_addr   = 0;
      intfc.app_wr_data    = 0;
      intfc.app_wr_en_n    = 4'hF;
      intfc.app_req_wr_n   = 0;
      intfc.app_req        = 0;
      intfc.app_req_len    = 0;
      intfc.RESETN         = 1'h1;
      #(delay_1);
      intfc.RESETN    = 1'h0;
      #(delay_2);
      intfc.RESETN    = 1'h1;
      #(delay_3);
      wait(intfc.sdr_init_done == 1);
      #(delay_4);
    end
  endtask
  
/***********************************************************
           Task to write in the SDRAM Normal mode
************************************************************/  
  
  task write(input logic [31:0] address, logic [31:0] data);
      begin
        $display("Executing Write\n");
        @ (negedge intfc.clk);
        /* Send data to DUT*/
          intfc.app_req        = 1;
          intfc.app_wr_en_n    = 0;
          intfc.app_req_wr_n   = 0;
          intfc.app_req_addr   = address;
          intfc.app_req_len    = 1;
       
          @(negedge intfc.app_req_ack);
          //@(negedge intfc.clk);
            intfc.app_req      = 0;
            intfc.app_wr_data = data;
   
          do begin
            @ (posedge intfc.clk);
          end while(intfc.app_wr_next_req == 1'b0);
            @ (negedge intfc.clk);
            $display("Write Address: %x  WriteData: %x",address,intfc.app_wr_data );

     /*Reset the data in DUT */ 
       intfc.app_req        = 0;
       intfc.app_wr_en_n    = 0;
       intfc.app_req_wr_n   = 0;
       intfc.app_req_addr   = 0;
       intfc.app_req_len    = 0;
       end
    $display("Finishing Write\n");
 endtask
  
/***********************************************************
                  Task to read in the SDRAM Normal mode
************************************************************/ 

  task read(input logic [31:0] address);
      begin
        $display("Executing read\n");
      /* Read Address from scoreboard*/
        @ (negedge intfc.clk);
      /*send data to DUT*/
        intfc.app_req        = 1;
        intfc.app_wr_en_n    = 0;
        intfc.app_req_wr_n   = 1;
        intfc.app_req_addr   = address;
        intfc.app_req_len    = 1;
        intfc.test = 1;
        
        @(negedge intfc.app_req_ack);
          @(negedge intfc.clk);
            intfc.app_req    = 0;
        
        wait(intfc.app_rd_valid == 1'b1);
        $display("Read Address: %x  Read Data: %x ", address, intfc.app_rd_data);
            intfc.test = 0;
            @ (posedge intfc.clk);
            @ (negedge intfc.clk);      
    end
    
    /*Reset the data in DUT */ 
    intfc.app_req        = 0;
    intfc.app_wr_en_n    = 0;
    intfc.app_req_wr_n   = 0;
    intfc.app_req_addr   = 0;
    intfc.app_req_len    = 0;
    $display("Finishing Read\n");
  endtask
  
/***********************************************************
           Task to write in the SDRAM Sequential mode
************************************************************/  
  
  task burst_write(input integer iteration);
    repeat(iteration)
      begin
        $display("Executing Burst Write\n");
        sti=new();
        @ (negedge intfc.clk);
        if(sti.randomize())
        /* Send data to DUT*/
          address = sti.startAddr;
          data   = sti.data;
          bl     = sti.bl;

          intfc.app_req        = 1;
          intfc.app_wr_en_n    = 0;
          intfc.app_req_wr_n   = 0;
          intfc.app_req_addr   = address;
          intfc.app_req_len    = bl;
          $display("Write Address: %x, Burst Size: %d",address,bl);
       
          @(negedge intfc.app_req_ack);
          @(negedge intfc.clk);
            intfc.app_req      = 0;
     
          for(i=0; i < bl; i++) begin
            intfc.app_wr_data = data;
            sb.dram_w[(address + i) & 32'hFFFFFFFF] = data; // Write data into the scoreboard
            
          do begin
            @ (posedge intfc.clk);
          end while(intfc.app_wr_next_req == 1'b0);
            @ (negedge intfc.clk);
            $display("Status: Burst-No: %d  Write Address: %x  WriteData: %x ",i,address,intfc.app_wr_data);
        end
        
     /*Reset the data in DUT */ 
       intfc.app_req        = 0;
       intfc.app_wr_en_n    = 0;
       intfc.app_req_wr_n   = 0;
       intfc.app_req_addr   = 0;
       intfc.app_req_len    = 0;
       end
       $display("Finishing Burst Write\n");
 endtask
  
/***********************************************************
                  Task to read in the SDRAM Sequential mode
************************************************************/ 

  task burst_read(input integer iteration);
    repeat(iteration)
      begin
        $display("Executing Burst read\n");
      /* Read Address from scoreboard*/
        @ (negedge intfc.clk);
        
      /*send data to DUT*/
        intfc.app_req        = 1;
        intfc.app_wr_en_n    = 0;
        intfc.app_req_wr_n   = 1;
        intfc.app_req_addr   = address;
        intfc.app_req_len    = bl;
        intfc.test = 1;
        $display("Read Address: %x, Burst Size: %d, Expected Data: %x",address, bl, data);
        
        @(negedge intfc.app_req_ack);
          @(negedge intfc.clk);
            intfc.app_req    = 0;
        
        for(j=0; j < bl; j++)
          begin
            
            wait(intfc.app_rd_valid == 1'b1);
            // Write data into the scoreboard
            sb.dram_r[(address + j) & 32'hFFFFFFFF] = intfc.app_rd_data;
            $display("Status: Burst-No: %d  Read Address: %x  Read Data: %x ",j, address, intfc.app_rd_data);
            intfc.test = 0;
            @ (posedge intfc.clk);
            @ (negedge intfc.clk);
          end        
    end
    
    /*Reset the data in DUT */ 
    intfc.app_req        = 0;
    intfc.app_wr_en_n    = 0;
    intfc.app_req_wr_n   = 0;
    intfc.app_req_addr   = 0;
    intfc.app_req_len    = 0;
    $display("Finishing Burst Read\n");
  endtask
  

endclass
