class environment;
  driver drvr;
  scoreboard sb;
  monitor mntr;
  coverage covr;
  virtual intf_sdrc_core intfc;
           
  function new(virtual intf_sdrc_core intfc);
    $display("Creating environment");
    this.intfc = intfc;
    sb = new();
    drvr = new(intfc,sb);
    mntr = new(intfc,sb);
    covr = new(intfc);
    fork 
      mntr.check();
    join_none
    
  endfunction
           
endclass
